﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace test_project_crypto1
{
    internal class PricePerTime
    {
        public long time { get; set; }
        public double DoublePriceUsd;
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
        private string _priceUsd;
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.

        public string priceUsd
        {
            get { return _priceUsd; }
            set
            {
                int dotIndex = value.IndexOf('.');

                if (double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out double parsedValue))
                {
                    _priceUsd = value;
                    DoublePriceUsd = parsedValue;
                }
                else
                {
                    DoublePriceUsd = 0;
                    _priceUsd = value;
                }
            }
        }

    }
}
