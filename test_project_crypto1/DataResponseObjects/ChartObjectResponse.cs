﻿using LiveCharts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_project_crypto1
{
    internal class ChartObjectResponse
    {
        public SeriesCollection PriceSeriesCollection = new SeriesCollection();
        public List<string> labels = new List<string>();
    }
}
