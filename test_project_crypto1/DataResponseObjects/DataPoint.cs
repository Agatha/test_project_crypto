﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace test_project_crypto1
{
    internal class DataPoint
    {
        public long UnixTimestamp { get; set; }
        public double Y { get; set; }

        private DateTime dateTime;
        public DateTime DateTime
        {
            get { return dateTime; }
            set
            {
                dateTime = value;
                UnixTimestamp = new DateTimeOffset(value).ToUnixTimeMilliseconds();
            }
        }

        public DataPoint(long unixTimestamp, double y)
        {
            UnixTimestamp = unixTimestamp;
            Y = y;
            DateTime = DateTimeOffset.FromUnixTimeMilliseconds(unixTimestamp).LocalDateTime;
        }
    }
}

