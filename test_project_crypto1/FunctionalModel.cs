﻿using LiveCharts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Documents;
using test_project_crypto1;

namespace test_project_crypto1
{
    internal class FunctionalModel : INotifyPropertyChanged
    {

        private readonly HttpClient client = new HttpClient();
#pragma warning disable CS8612 // Допустимость значения NULL для ссылочных типов в типе не совпадает с явно реализованным членом.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8612 // Допустимость значения NULL для ссылочных типов в типе не совпадает с явно реализованным членом.

        public async Task<ObservableCollection<Currency>> GetCurrenciesByRank(bool top10)
        {
            var response = await client.GetAsync(CoinCap.Url + CoinCap.ByRank);
            string content = await response.Content.ReadAsStringAsync();
            var currensies = DeserialiseJSONToCurrensies(content);
            if (top10)
            {
                return new ObservableCollection<Currency>(currensies.data.Take(10));
            }
            else
            {
                return new ObservableCollection<Currency>(currensies.data);
            }
            
        }

        public async Task <List<PricePerTime>> GetPricePerTimeForGraphics(Currency currency, SeriesCollection values)
        {
            var response = await client.GetAsync(CoinCap.Url + CoinCap.ByRank+"/"+ currency.id + CoinCap.History+ "?interval=d1");
            string content = await response.Content.ReadAsStringAsync();
            var currensies = DeserialiseJSONToPricePerTime(content);
            return currensies.data.ToList<PricePerTime>();

        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private RankedCurrensyResponse  DeserialiseJSONToCurrensies(string jsonString)
        {
            var response = JsonSerializer.Deserialize<RankedCurrensyResponse>(jsonString);

#pragma warning disable CS8603 // Возможно, возврат ссылки, допускающей значение NULL.
           return response;
#pragma warning restore CS8603 // Возможно, возврат ссылки, допускающей значение NULL.

        }

        private PricePerTimeResponse DeserialiseJSONToPricePerTime(string jsonString)
        {
            var response = JsonSerializer.Deserialize<PricePerTimeResponse>(jsonString);

#pragma warning disable CS8603 // Возможно, возврат ссылки, допускающей значение NULL.
            return response;
#pragma warning restore CS8603 // Возможно, возврат ссылки, допускающей значение NULL.

        }
    }
}
