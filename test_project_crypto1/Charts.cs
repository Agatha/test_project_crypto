﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using test_project_crypto1;

namespace test_project_crypto1
{
    internal class Charts
    {
        public async Task<ChartObjectResponse> GetDataForPriceChart(Currency currensy, FunctionalModel model)
        {
            var chartObjectResponse = new ChartObjectResponse();
            List<DataPoint> dataPoints = new List<DataPoint>();

            var pricePerTimeDataSource = await model.GetPricePerTimeForGraphics(currensy, chartObjectResponse.PriceSeriesCollection);
            foreach (var item in pricePerTimeDataSource)
            {
                dataPoints.Add(new DataPoint(item.time, item.DoublePriceUsd));
            }

            ChartValues<DateTimePoint> values = new ChartValues<DateTimePoint>();
            foreach (var point in dataPoints)
            {
                values.Add(new DateTimePoint(point.DateTime, point.Y));
            }

            chartObjectResponse.PriceSeriesCollection.Add(new LineSeries { Values = values });
            return chartObjectResponse;
        }
    }
}
