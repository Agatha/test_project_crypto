﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_project_crypto1
{
    internal class TabItemVM
    {
        public class TabItemV : ViewModel
        {
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
            private string header;
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
            public string Header
            {
                get { return header; }
                set
                {
                    header = value;
                    OnPropertyChanged("Header");
                }
            }

        }
    }
}
