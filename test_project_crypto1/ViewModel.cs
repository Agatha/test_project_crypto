﻿using LiveCharts.Defaults;
using LiveCharts.Wpf;
using LiveCharts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using LiveCharts.Events;
using System.Windows.Controls;
using System.Globalization;
using System.Windows.Data;
using System.Diagnostics;
using test_project_crypto1;

namespace test_project_crypto1
{
    //internal delegate string DateTimeLabelFormatter(double value);
    internal class ViewModel : INotifyPropertyChanged
    {

        private Currency selectedCurrency;
        private ObservableCollection<Currency> allCurrency;
        private ObservableCollection<Currency> savedAllCurrency;
        private bool top10;
        private Charts Charts;
        private TabItemVM selectedTab;
        private List<string> labels = new List<string>();
        private string _searchText;
       
        public ICommand OpenMarketCommand { get; }   
        public ICommand Top10ChangedCommand { get; }
        public ICommand SearchCommand { get; }
       
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    OnPropertyChanged("SearchText");
                    Top10ChangedCommand.Execute(SearchText);
                }
            }
        }
        public FunctionalModel Model;
        public SeriesCollection SeriesCollectionPriceChart { get; set; }
        public Func<double, string> Formatter { get; set; }
        public ObservableCollection<TabItemVM> TabItems { get; private set; }

#pragma warning disable CS8612 // Допустимость значения NULL для ссылочных типов в типе не совпадает с явно реализованным членом.
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS8612 // Допустимость значения NULL для ссылочных типов в типе не совпадает с явно реализованным членом.

        public TabItemVM SelectedTab
        {
            get { return selectedTab; }
            set
            {
                selectedTab = value;
                OnPropertyChanged(nameof(SelectedTab));
            }
        }

        public ObservableCollection<Currency> AllCurrency
        {
            get { return allCurrency; }
            set
            {
                allCurrency = value;
                OnPropertyChanged("AllCurrency");
            }
        }
        public Currency SelectedCurrency
        {
            get { return selectedCurrency; }
            set
            {
                selectedCurrency = value;
                OnPropertyChanged("SelectedCurrency");
                GetChartInformation(SelectedCurrency, Model);
            }
        }

        public List<string> Labels
        {
            get { return labels; }
            set
            {
                labels = value;
                OnPropertyChanged("Labels");
            }
        }

        public bool Top10
        {
            get { return top10; }
            set
            {
                if (top10 != value)
                {
                    top10 = value;
                    OnPropertyChanged("Top10");
                    Top10ChangedCommand.Execute(Top10);
                }
            }
        }

#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning disable CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
        internal ViewModel()
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
#pragma warning restore CS8618 // Поле, не допускающее значения NULL, должно содержать значение, отличное от NULL, при выходе из конструктора. Возможно, стоит объявить поле как допускающее значения NULL.
        {
            Top10ChangedCommand = new RelayCommand(OnIsTop10Changed);
            SearchCommand = new GenericRelayCommand<string>(FilterCurrency);
            OpenMarketCommand = new RelayCommand(OpenMarket);
            Formatter = value => new DateTime((long)value).ToString("yyyy-MM-dd");
            TabItems = new ObservableCollection<TabItemVM>();
            Top10 = true;
        }

        private void OnIsTop10Changed(object param)
        {
            if (param is bool top10)
            {
                GetStartInformation(top10);
            }
        }

        private async void GetStartInformation(bool top10)
        {
            Model = new FunctionalModel();
            Charts = new Charts();

            AllCurrency = await Model.GetCurrenciesByRank(top10);
            SelectedCurrency = AllCurrency[0];
          
                GetChartInformation(SelectedCurrency, Model);
        }

        private async void GetChartInformation(Currency selectedCurrency, FunctionalModel Model)
        {
            if (!(SelectedCurrency is null))
            {
                var chartObjectResponse = await Charts.GetDataForPriceChart(SelectedCurrency, Model);
                SeriesCollectionPriceChart = chartObjectResponse.PriceSeriesCollection;
                OnPropertyChanged("SeriesCollectionPriceChart");
            }

        }
        private void FilterCurrency(string searchText)
        {
            if (string.IsNullOrWhiteSpace(searchText))
            {
                AllCurrency = savedAllCurrency;
            }
            else 
            {   
                savedAllCurrency ??= AllCurrency;

                AllCurrency = new ObservableCollection<Currency>(
                AllCurrency.Where(currency =>
                currency.name.Contains(searchText, StringComparison.OrdinalIgnoreCase) ||
                currency.id.Contains(searchText, StringComparison.OrdinalIgnoreCase)));
            }
        }

        private void OpenMarket(object param)
        {
            if (SelectedCurrency != null)
            {
                string url = SelectedCurrency.explorer;
                if (!string.IsNullOrEmpty(SelectedCurrency.explorer))
                {
                    try
                    {
                        Process.Start(new ProcessStartInfo(SelectedCurrency.explorer)
                        {
                            UseShellExecute = true
                        });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}

