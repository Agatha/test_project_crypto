﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_project_crypto1
{
    internal class CoinCap
    {
        public const string Url = "https://api.coincap.io/v2";
        public const string ByRank = "/assets";
        public const string History = "/history";
    }
}
