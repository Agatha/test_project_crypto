﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using LiveCharts.Wpf;

namespace test_project_crypto1
{
    internal class LabelsToAnnotationsConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is List<string> labels)
            {
                var annotations = new List<LabelAnnotation>();
                for (int i = 0; i < labels.Count; i++)
                {
                    annotations.Add(new LabelAnnotation
                    {
                        Text = labels[i],
                        Position = i
                    });
                }
                return annotations;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
}
