# test_project_crypto



## implemented functionality
- The main page displays the top 10 or ALL currencies by popularity that were returned by API and detail information about selected currency. We can searching currency in list by name or code. And we can to go to the currency page on the market.
- The second page displays price chart (LiveCharts library was used) for selected currency.

The application implements the pattern MVVM.

The C# language, .NET 6.0 and WPF were used.



